
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var winston = require('winston');
require('winston-logstash');

winston.add(winston.transports.Logstash, {
  port: 28777,
  node_name: 'Winston_Node_Sample',
  host: '192.168.9.55'
});

winston.log('info', 'This is the Logstash test - Info');
winston.log('debug', 'This is the Logstash test - Debug');
winston.log('warn', 'This is the Logstash test - Warn');
winston.log('trace', 'This is the Logstash test - Trace');
winston.log('error', 'This is the Logstash test - Error');

